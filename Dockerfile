FROM golang:1.22.0-alpine3.18@sha256:68fcc123b72e6f76ccee9680522217ba7f33a19b37f7d92ba1d8a10d483d5745

# hadolint ignore=DL3018
RUN apk add --no-cache git gcc musl-dev && \
  go install github.com/boumenot/gocover-cobertura@v1.0.0
