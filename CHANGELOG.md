# Changelog

## 1.0.1

- Pin `foo` preset at v1.0.0.

## 1.0.0

Initial release.
